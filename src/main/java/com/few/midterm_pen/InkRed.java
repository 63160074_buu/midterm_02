/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.midterm_pen;

/**
 *
 * @author f
 */
public class InkRed extends Pen {

    public InkRed(String penColor, double nibSize) {
        super(penColor, nibSize);
        System.out.println("Create Red pen");
    }

    @Override
    public void showPen() {
        System.out.println("Pen color: " + penColor);
        System.out.println("Ink color: Red");
        System.out.println("Nib color: " + nibSize);
        System.out.println("_____________________");
    }
}
