/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.midterm_pen;

/**
 *
 * @author f
 */
public class InkBlue extends Pen {
     public InkBlue(String penColor, double nibSize) {
        super(penColor, nibSize);
        System.out.println("Create Blue pen");
    }
     @Override
    public void showPen() {
        System.out.println("Pen color: " + penColor);
        System.out.println("Ink color: Blue");
        System.out.println("Nib color: " + nibSize);
        System.out.println("_____________________");
    }
}
