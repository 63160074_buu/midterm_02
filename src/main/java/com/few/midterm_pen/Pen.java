/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.midterm_pen;

/**
 *
 * @author f
 */
public class Pen {

    protected String penColor;
    protected double nibSize;//nib = ขนาดปลายปากกา

    public Pen(String penColor, double nibSize) {
        System.out.println("Create Pen");
        this.penColor = penColor;
        this.nibSize = nibSize;
    }

    public void showPen() {
        System.out.println("Pen color: " + penColor);
        System.out.println("Nib size: " + nibSize);
        System.out.println("_____________________");
    }

    public String getPenColor() {
        return penColor;
    }

    public double getNibSize() {
        return nibSize;
    }
}
