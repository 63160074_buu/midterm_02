/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.midterm_pen;

/**
 *
 * @author f
 */
public class MainProgram {

    public static void main(String[] args) {
        Pen pen = new Pen("Black", 0.5);
        pen.showPen();
        InkRed red = new InkRed("Red", 0.5);
        red.showPen();
        InkBlue blue = new InkBlue("Blue", 0.5);
        blue.showPen();
        InkBlack black = new InkBlack("Black", 0.5);
        black.showPen();
    }
}
